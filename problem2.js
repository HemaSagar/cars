function problem2(inventory){

    const data = inventory[inventory.length-1];

    return "Last car is a " + data.car_make + " " + data.car_model;
}


module.exports = problem2;
// console.log(problem2(inventory))
//"Last car is a *car make goes here* *car model goes here*"