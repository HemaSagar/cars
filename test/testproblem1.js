const problem1 = require('../problem1.js')
const inventory = require('../cars.js')


const result = problem1(inventory,33);
if (result.length==0){
    console.log(result);
}
else{
    console.log("car " + result.id + " is a " + result.car_year + " " + result.car_make + " " + result.car_model)
}