##Hi welcome to cars project

Files in this Project
    - problem1.js
    - problem2.js
    - problem3.js
    - problem4.js
    - problem6.js
    - problem5.js
    - problem6.js

It contains a test folder in which each **testprogram** reveals the output of that particular program.
Program has been imported into the testprogram file individually and console.log the output to the screen.

The **cars.js** file contain the information about the cars in an array format.
This array file is exported and being imported wherever necessary.
