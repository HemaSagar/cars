const olderCars = [];
function problem5(inventory,carYears){
    
    for(i=0; i<carYears.length;i++){
        if(carYears[i] < 2000){
            olderCars.push(carYears[i]);
        }
    }

    return olderCars;
}

module.exports = problem5;